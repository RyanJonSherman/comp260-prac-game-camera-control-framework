﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowLock : MonoBehaviour {

    public Transform target;
    public Vector3 zOffset = new Vector3(0, 0, -10);

    public float maxSpeed = 5.0f;
    public float maxOffset = 2.0f;

    public float lerpFactor = 0.5f;

    private Rigidbody2D targetRigidbody;
    private Vector3 xyOffset = Vector3.zero;

    private void Start()
    {
        targetRigidbody = target.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        Vector3 velocity = targetRigidbody.velocity;

        xyOffset = Vector3.Lerp(xyOffset, velocity * maxOffset / maxSpeed, 1 - Mathf.Pow(1 - lerpFactor, Time.deltaTime));

        transform.position = target.position + xyOffset + zOffset;
    }
}
