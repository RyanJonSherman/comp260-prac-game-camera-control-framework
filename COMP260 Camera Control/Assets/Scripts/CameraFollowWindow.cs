﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowWindow : MonoBehaviour {

    public Transform target;
    public Vector3 zOffset = new Vector3(0, 0, -10);
    public Rect window = new Rect(0, 0, 1, 1);

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        for (int i = 0; i < 4; i++)
        {
            Vector3 f = Camera.main.ViewportToWorldPoint(window.Corner(i)) - zOffset;
            Vector3 t = Camera.main.ViewportToWorldPoint(window.Corner(i+1)) - zOffset;

            Gizmos.DrawLine(f, t);
        }
    }

    private void Update()
    {
        Vector3 targetPos = target.position;

        Vector2 targetViewPos = Camera.main.WorldToViewportPoint(targetPos);

        Vector2 goalViewPos = window.Clamp(targetViewPos);

        Vector3 goalPos = Camera.main.ViewportToWorldPoint(goalViewPos);

        targetPos = transform.InverseTransformPoint(targetPos);
        goalPos = transform.InverseTransformPoint(goalPos);

        Vector3 move = targetPos - goalPos;
        move.z = 0;

        transform.Translate(move);
    }
}
