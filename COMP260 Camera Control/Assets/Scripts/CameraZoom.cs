﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour {

    public Transform targetA;
    public Transform targetB;

    public Vector3 zOffset = new Vector3(0, 0, -10);

    public float maxSpeed = 5.0f;
    public float maxOffset = 2.0f;

    public float minSize = 3.0f;

    public float lerpFactor = 0.5f;

    private Rigidbody2D targetRigidbody;
    private Vector3 xyOffset = Vector3.zero;

    private Camera camera;

    private void Start()
    {
        camera = gameObject.GetComponent<Camera>();
        camera.orthographic = true;
    }

    private void Update()
    {
        Vector3 target = (targetA.position + targetB.position) / 2;

        transform.position = target + zOffset;

        camera.orthographicSize = Mathf.Max(Vector3.Distance(targetA.position, targetB.position), minSize);
    }
}
